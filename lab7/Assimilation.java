import java.io.*;
import java.util.*;
import java.util.Scanner;
import java.util.stream.*;

class Assimilation implements Serializable {
	
	private List<Borg> borgCollective;
	
	public List<Borg> getBorg() {
		return borgCollective;
	}
	
	
	public void assimilate(List<Borg> newBorg) {
		this.borgCollective.addAll(newBorg);
	}
	
	public Assimilation(List<Borg> previousBorg) {
		this.borgCollective = new ArrayList<Borg>(previousBorg);
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		
		// if Borg already exists, get it by deserializing and 
		// use as constructor's parameter
		Assimilation assimilation = new Assimilation(new ArrayList<Borg>());
		
		
		System.out.println("First, mankind had developed and explored Universe...");
		List<Individual> mankind = new ArrayList<Individual>();
		
		Scanner sc = new Scanner(new File(args[0]));
		// System.out.println("Getting data from " + args[0]);
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			// System.out.println(line);
			mankind.add(new Individual(line));
		}
		sc.close();
	
		System.out.println();
		System.out.println("These were the people of Earth:");
		// System.out.println("===============================");
		
		mankind.forEach(id -> System.out.
			printf("%s, value %d%n", id, id.getStatus().value()));

		System.out.println();
		System.out.println("Then came the Borg, removed the \"unworthy\"" +
					" and assimilated the rest (quick and easy)");

		List<Borg> newBorgMembers = mankind.stream()
				.filter(id -> id.getAge() < 21 || id.getStatus().value() > 35)
				.map(Borg::new)
				.collect(Collectors.toList());

		assimilation.assimilate(newBorgMembers);
		System.out.println();
		System.out.println("This are the Borg:");
		for (Borg b: assimilation.borgCollective)
			System.out.println(b);
		System.out.println();
		System.out.println(Borg.seeBorg());
	}
	
}
