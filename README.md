This is a master repository to host home works 7 and 8, and assignment two for 
Introductory Programming (in Java) course, COMP2140/6700, in 2016

You should fork it and add your lecturers and tutor:

- Henry Gardner
- Alexei Khorev
- Charles Martin

as reporters (so they can clone your repository for marking after you push
your work into this repository for submission).

Each of the three items of work -- Homework 7, Homework 8 and Assignment 2 --
have files which contain further explanations (hw7.md, hw8.md and ass2.md,
correspondingly). More detailed explanations should be found on the appropriate
web pages of the course web site https://cs.anu.edu.au/courses/comp6700/ .